<?php

namespace Survey\SurveyPage\Controller\Result;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;

class Result extends Action
{
    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        /** @var Messages $messageBlock */
        $messageBlock = $resultPage->getLayout()->createBlock(
            'Magento\Framework\View\Element\Messages',
            'answer'
        );

        // product IDS 1-5
        $product_1 = $this->getRequest()->getParam('1');
        $product_2 = $this->getRequest()->getParam('2');
        $product_3 = $this->getRequest()->getParam('3');
        $product_4 = $this->getRequest()->getParam('4');
        $product_5 = $this->getRequest()->getParam('5');

        if($product_1 < 1 || 
            $product_2 < 1 || 
            $product_3 < 1 || 
            $product_4 < 1 || 
            $product_5 < 1) {
            $messageBlock->addError('Please answer for all products');
        } else {
            $messageBlock->addSuccess('Thank you for answering');
        }


        $resultPage->getLayout()->setChild(
            'content',
            $messageBlock->getNameInLayout(),
            'answer_alias'
        );

        return $resultPage;
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

