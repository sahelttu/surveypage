<?php

namespace Survey\SurveyPage\Api\Data;

interface PostInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ANSWER_ID               = 'answer_id';
    const ANSWER_VALUE            = 'answer_value';
    const ANSWER_QUESTION_ID      = 'question_id';
    /**#@-*/


    /**
     * Set answer ID
     *
     * @return string|null
     */
    public function setAnswerId($id);

    /**
     * Set answer value
     *
     * @return int|null
     */
    public function setAnswerValue($value);

     /**
     * Set answer question id
     *
     * @return int|null
     */
    public function setAnswerQuestionId($id);

    /**
     * Get answer ID
     *
     */
    public function getAnswerId();

    /**
     * Get answer value
     *
     */
    public function getAnswerValue();

    /**
     * Get answer question id
     *
     */
    public function getAnswerQuestionId();
}
