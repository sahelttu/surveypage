<?php

namespace Survey\SurveyPage\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Survey\SurveyPage\Model\ResourceModel\Post\Collection as AnswerCollection;
use \Survey\SurveyPage\Model\ResourceModel\Post\CollectionFactory as AnswerCollectionFactory;
use \Survey\SurveyPage\Model\Post;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SortOrderBuilder;

class Posts extends Template
{
    /**
     * CollectionFactory
     * @var null|CollectionFactory
     */
    protected $_postCollectionFactory = null;

    /**
     * Constructor
     *
     * @param Context $context
     * @param AnswerCollectionFactory $answerCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        AnswerCollectionFactory $answerCollectionFactory,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        array $data = []
    ) {
        $this->_answerCollectionFactory = $answerCollectionFactory;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        parent::__construct($context, $data);
    }

    /**
     * @return Answer[]
     */
    public function getAnswers()
    {
        $answerCollection = $this->_answerCollectionFactory->create();
        $answerCollection->addFieldToSelect('*')->load();
        return $answerCollection->getItems();
    }


    /**
     * Get list of the 5 heaviest products
     *
     * @return ProductInterface[]
     */
    public function getProducts()
    {

        // Get the first 5 products
        $this->searchCriteriaBuilder
            ->setPageSize(5)
            ->setCurrentPage(1);

        // Create the SearchCriteria
        $searchCriteria = $this->searchCriteriaBuilder->create();


        // Load the products
        $products = $this->productRepository
            ->getList($searchCriteria)
            ->getItems();

        return $products;
    }

}
