<?php

namespace Survey\SurveyPage\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;
use \Survey\SurveyPage\Api\Data\PostInterface;

/**
 * Class File
 * @package Toptal\Blog\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Post extends AbstractModel implements PostInterface, IdentityInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'survey_answer';

    /**
     * Post Initialization
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Survey\SurveyPage\Model\ResourceModel\Post');
    }


    /**
     * Get answer value
     *
     * @return string|null
     */
    public function getAnswerValue()
    {
        return $this->getData(self::ANSWER_VALUE);
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getAnswerId()];
    }

    /**
     * Get answer ID
     *
     * @return string|null
     */
    public function getAnswerId()
    {
        return $this->getData(self::ANSWER_ID);
    }

    /**
     * Get answer ID
     *
     * @return string|null
     */
    public function getAnswerQuestionId()
    {
        return $this->getData(self::ANSWER_QUESTION_ID);
    }

    /**
     * Set AnswerValue
     *
     * @param string $value
     * @return $this
     */
    public function setAnswerValue($value)
    {
        return $this->setData(self::ANSWER_VALUE, $value);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setAnswerId($id)
    {
        return $this->setData(self::ANSWER_ID, $id);
    }

    /**
     * Set question ID
     *
     * @param int $id
     * @return $this
     */
    public function setAnswerQuestionId($id)
    {
        return $this->setData(self::QUESTION_ID, $id);
    }
}
